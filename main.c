// Daniel Wimmer
// SDS394 Scientific and Technical Computing
// Fall 2016
// Calculate Pi HW using OpenMP

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

void numerical_integration(int size);
void adaptive_integration(int size);
void monte_carlo(int size);

int main()
{
   int size = (int)pow(10,9);

// Determine which is more accurate and which is faster
// For each, use sample sizes of 10^4, 10^6, and 10^9

// Use parallel threads to calculate Pi using numerical integration method
   numerical_integration(size);

// Use parallel threads to calculate Pi using adaptive integration method
   adaptive_integration(size);

// Use parallel threads to calculate Pi using Monte Carlo method
   monte_carlo(size);
}

void numerical_integration(int size)
{
   double piNI = 0;
   double x,term;
   int i;
   double tstart,tend,elapsed;

   #pragma omp parallel num_threads(16)
   {
      tstart = omp_get_wtime();
      for(i=0;i<size;i++)
      {
         x = (double)rand() / (double)RAND_MAX;
         term = 1 - pow(x,2);
         term = pow(term,0.5);
         piNI = piNI + (4 * term) / (double)size;
      }
      tend = omp_get_wtime();
      elapsed = tend - tstart;
   } 
   printf("\nSample size: %i", size);
   printf("\nNumerical Integration Time: %.5f\n", elapsed);
   printf("\nNumerical Integration PI: %.10f\n", piNI);
}

void adaptive_integration(int size)
{
   double piAI = 0;
   double h = 1.0 / (double)size;
   double x,term;
   int i;
   double tstart,tend,elapsed;

   #pragma omp parallel num_threads(16)
   {
      tstart = omp_get_wtime();
   
     #pragma omp parallel for schedule(guided) reduction(+:piAI) 
      for(i=1;i<size-1;i++)
      {
         x = (double)i*h;
         term = 1 - pow(x,2);
         term = pow(term,0.5);
         piAI = piAI + 4 * term * h;
      }
      tend = omp_get_wtime();
      elapsed = tend - tstart;
   }
   printf("\nAdaptive Integration Time: %.5f\n", elapsed);
   printf("\nAdaptive Integration PI: %.10f\n", piAI);
}

void monte_carlo(int size)
{
   double piMC = 0;
   double x,y;
   double tstart,tend,elapsed;
   int i;
   int inside = 0;

   #pragma omp parallel num_threads(16)
   {
      tstart = omp_get_wtime();

      for(i=0;i<size;i++)
      {
         x = (double)rand() / (double)RAND_MAX;
         y = (double)rand() / (double)RAND_MAX;
         if(x*x + y*y < 1)
            inside = inside + 1;
      }
     
      piMC = (4 * (double)inside) / (double)size;
      tend = omp_get_wtime();
      elapsed = tend - tstart;
   }
   printf("\nMonte Carlo Time: %.5f\n", elapsed);
   printf("\nMonte Carlo PI: %.10f\n", piMC);
}
